<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\BackendController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// frontend routes
Route::controller(FrontendController::class)->group(function (){

    Route::get('/', 'index')->name('home');
    Route::get('/category-product', 'category')->name('category');
    Route::get('/product-details', 'porductDetails')->name('products-details');
    Route::get('/cart', 'cart')->name('add-to-cart');
    Route::get('/checkout',  'checkout')->name('checkout');
    Route::get('/order',  'order')->name('order');

});


// backend routes
Route::get('/login', [BackendController::class, 'login']);
Route::get('/registration', [BackendController::class, 'registration']);

    Route::prefix('admin/')->group(function () {

        Route::get('/', [BackendController::class, 'index'])->name('dashbord');
    
        Route::controller(ProductController::class)->group(function () {

            Route::get('/products', 'product')->name('product');
            Route::get('/products-Add', 'create')->name('product.add');
            Route::post('/products', 'store')->name('product');
        });
    });






Route::fallback(function () {
    return "404";
});
