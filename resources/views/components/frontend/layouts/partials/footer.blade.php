<footer class="footer appear-animate" data-animation-options="{ 'delay': '.2s' }">
            <div class="footer-middle">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget widget-about">
                                <a href="demo7.html" class="logo-footer mb-5">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/footer-logo.png" alt="logo-footer" width="154"
                                        height="43" />
                                </a>
                                <div class="widget-body">
                                    <p>Fringilla urna porttitor rhoncus dolor purus<br />
                                        luctus venenatis lectus magna fringilla diam<br />
                                        maecenas ultricies mi eget mauris.</p>
                                    <a href="mailto:mail@riode.com">Riode@example.com</a>
                                </div>
                            </div>
                            <!-- End of Widget -->
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget">
                                <h4 class="widget-title">Account</h4>
                                <ul class="widget-body">
                                    <li><a href="account.html">My Account</a></li>
                                    <li><a href="#">Our Guarantees</a></li>
                                    <li><a href="#">Terms And Conditions</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Intellectual Property Claims</a></li>
                                    <li><a href="#">Site Map</a></li>
                                </ul>
                            </div>
                            <!-- End of Widget -->
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="widget mb-6 mb-sm-0">
                                <h4 class="widget-title">Get Help</h4>
                                <ul class="widget-body">
                                    <li><a href="#">Shipping &amp; Delivery</a></li>
                                    <li><a href="#">Order Status</a></li>
                                    <li><a href="#">Brand</a></li>
                                    <li><a href="#">Returns</a></li>
                                    <li><a href="#">Payment Options</a></li>
                                    <li><a href="contact-us.html">Contact Us</a></li>
                                </ul>
                            </div>
                            <!-- End of Widget -->
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="widget">
                                <h4 class="widget-title text-normal">Subscribe to Our Newsletter</h4>
                                <div class="widget-body widget-newsletter">
                                    <form action="#" class="input-wrapper input-wrapper-inline">
                                        <input type="email" class="form-control" name="email" id="email"
                                            placeholder="Email address here..." required />
                                        <button class="btn btn-primary btn-sm btn-icon-right btn-rounded"
                                            type="submit">subscribe<i class="d-icon-arrow-right"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div class="footer-info">
                                <figure class="payment">
                                    <img src="{{ asset('frontend') }}/images/demos/demo4/payment.png" alt="payment" width="135" height="24" />
                                </figure>
                                <div class="social-links">
                                    <a href="#" title="social-link"
                                        class="social-link social-facebook fab fa-facebook-f"></a>
                                    <a href="#" title="social-link"
                                        class="social-link social-twitter fab fa-twitter"></a>
                                    <a href="#" title="social-link"
                                        class="social-link social-linkedin fab fa-linkedin-in"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of FooterMiddle -->
            <div class="footer-bottom d-block text-center">
                <p class="copyright">Riode eCommerce &copy; 2021. All Rights Reserved</p>
            </div>
            <!-- End of FooterBottom -->
        </footer>