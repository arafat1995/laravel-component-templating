<section class="mt-10 pt-7 appear-animate" data-animation-options="{
                    'delay': '.2s'
                }">
                <div class="container">
                    <h2 class="title title-center">Best Sellers</h2>
                    <div class="row">
                        <div class="col-lg-4 col-6 mb-4">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="{{ route('products-details') }}">
                                        <img src="{{ asset('frontend') }}/images/demos/demo7/products/1.jpg" alt="product" width="500" height="345">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="{{ route('products-details') }}">Nike Training Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="{{ route('products-details') }}" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{ asset('frontend') }}/images/demos/demo7/products/2.jpg" href="#" style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/1.jpg" href="#" style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/3.jpg" href="#" style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 mb-4">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="{{ route('products-details') }}">
                                        <img src="{{ asset('frontend') }}/images/demos/demo7/products/2.jpg" alt="product" width="500" height="345">
                                    </a>
                                    <div class="product-label-group">
                                        <label class="product-label label-new">new</label>
                                    </div>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="{{ route('products-details') }}">Classical Utility Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="{{ route('products-details') }}" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{ asset('frontend') }}/images/demos/demo7/products/3.jpg" href="#" style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/2.jpg" href="#" style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/1.jpg" href="#" style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 mb-4">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="{{ route('products-details') }}">
                                        <img src="{{ asset('frontend') }}/images/demos/demo7/products/3.jpg" alt="product" width="500" height="345">
                                    </a>
                                    <div class="product-label-group">
                                        <label class="product-label label-new">new</label>
                                    </div>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="{{ route('products-details') }}">Fashion NIKE Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$123.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="{{ route('products-details') }}" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{ asset('frontend') }}/images/demos/demo7/products/4.jpg" href="#" style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/2.jpg" href="#" style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/3.jpg" href="#" style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 mb-4">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="{{ route('products-details') }}">
                                        <img src="{{ asset('frontend') }}/images/demos/demo7/products/4.jpg" alt="product" width="500" height="345">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="{{ route('products-details') }}">Skyblue Basketball Boots</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$323.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="{{ route('products-details') }}" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{ asset('frontend') }}/images/demos/demo7/products/5.jpg" href="#" style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/2.jpg" href="#" style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/3.jpg" href="#" style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 mb-4">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="{{ route('products-details') }}">
                                        <img src="{{ asset('frontend') }}/images/demos/demo7/products/5.jpg" alt="product" width="500" height="345">
                                    </a>
                                    <div class="product-label-group">
                                        <label class="product-label label-new">New</label>
                                    </div>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="{{ route('products-details') }}">Fashion Nike Training Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$234.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="{{ route('products-details') }}" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{ asset('frontend') }}/images/demos/demo7/products/1.jpg" href="#" style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/2.jpg" href="#" style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/3.jpg" href="#" style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 mb-4">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="{{ route('products-details') }}">
                                        <img src="{{ asset('frontend') }}/images/demos/demo7/products/6.jpg" alt="product" width="500" height="345">
                                    </a>
                                    <div class="product-label-group">
                                        <label class="product-label label-new">new</label>
                                    </div>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                            View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="{{ route('products-details') }}">Blue Suede Training Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="price">$563.00</span>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="{{ route('products-details') }}" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{ asset('frontend') }}/images/demos/demo7/products/1.jpg" href="#" style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/2.jpg" href="#" style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{ asset('frontend') }}/images/demos/demo7/products/3.jpg" href="#" style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>