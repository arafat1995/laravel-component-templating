<section class="mt-10 pt-7 appear-animate" data-animation-options="{
                    'delay': '.2s'
                }">
                <div class="container">
                    <h2 class="title title-center mb-6">Featured Item</h2>
                    <div class="product product-single row pt-4">
                        <div class="col-md-6 product-gallery">
                            <div class="rotate-slider owl-carousel product-single-carousel owl-theme owl-nav-arrow row gutter-no cols-1" data-owl-options="{
                                    'nav': true,
                                    'dots': false,
                                    'loop': true
                                }">
                                <figure class="product-image">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/products/big1.jpg" data-zoom-image="{{ asset('frontend') }}/images/demos/demo7/products/big1.jpg" alt="Women's Brown Leather Shoes1" width="800" height="800">
                                </figure>
                                <figure class="product-image">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/products/big2.jpg" data-zoom-image="{{ asset('frontend') }}/images/demos/demo7/products/big2.jpg" alt="Women's Brown Leather Shoes2" width="800" height="800">
                                </figure>
                                <figure class="product-image">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/products/big1.jpg" data-zoom-image="{{ asset('frontend') }}/images/demos/demo7/products/big1.jpg" alt="Women's Brown Leather Shoes3" width="800" height="800">
                                </figure>
                                <figure class="product-image">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/products/big2.jpg" data-zoom-image="{{ asset('frontend') }}/images/demos/demo7/products/big2.jpg" alt="Women's Brown Leather Shoes4" width="800" height="800">
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product-details">
                                <h2 class="product-name">
                                    <a href="{{ route('products-details') }}">North Face Training Wear</a>
                                </h2>
                                <div class="product-meta mb-3">
                                    SKU: <span class="product-sku">12345672</span>
                                    CATEGORY: <span class="product-cat text-capitalize">The North Face</span>
                                </div>
                                <div class="product-price">
                                    <span class="price">$130.00</span>
                                </div>
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width:80%"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="{{ route('products-details') }}" class="rating-reviews">( 1 reviews )</a>
                                </div>

                                <p class="product-short-desc font-primary">
                                    Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus
                                    metus
                                    libero eu augue. Morbi purus liberpuro ate vol faucibus adipiscing.</p>
                                <hr class="product-divider">

                                <div class="product-form product-qty">
                                    <div class="product-form-group">
                                        <div class="input-group mr-2">
                                            <button class="quantity-minus d-icon-minus"></button>
                                            <input class="quantity form-control" type="number" min="1" max="1000000">
                                            <button class="quantity-plus d-icon-plus"></button>
                                        </div>
                                        <button class="btn-product btn-cart text-normal ls-normal font-weight-semi-bold"><i class="d-icon-bag"></i>Add to
                                            Cart</button>
                                    </div>
                                </div>

                                <hr class="product-divider mb-3">

                                <div class="product-footer">
                                    <div class="social-links mr-4">
                                        <a href="#" title="social-link" class="social-link social-facebook fab fa-facebook-f"></a>
                                        <a href="#" title="social-link" class="social-link social-twitter fab fa-twitter"></a>
                                        <a href="#" title="social-link" class="social-link social-pinterest fab fa-pinterest-p"></a>
                                    </div>
                                    <span class="divider d-lg-show"></span>
                                    <a href="#" class="btn-product btn-wishlist mr-6"><i class="d-icon-heart"></i>Add to
                                        wishlist</a>
                                    <a href="#" class="btn-product btn-compare"><i class="d-icon-compare"></i>Add
                                        to
                                        compare</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>