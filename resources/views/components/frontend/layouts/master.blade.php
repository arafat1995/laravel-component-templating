<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title>Riode - {{ $title }}</title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Riode - Ultimate eCommerce Template">
    <meta name="author" content="D-THEMES">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('frontend') }}/images/icons/favicon.png">
    <!-- Preload Font -->
    <link rel="preload" href="{{ asset('frontend') }}/fonts/riode.ttf?5gap68" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" href="{{ asset('frontend') }}/vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" href="{{ asset('frontend') }}/vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2"
        crossorigin="anonymous">
    <script>
        WebFontConfig = {
            google: { families: [ 'Poppins:400,500,600,700,800,900' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = 'js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script>

    @stack('cart-css')

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/vendor/animate/animate.min.css">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/vendor/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/vendor/owl-carousel/owl.carousel.min.css">
    @stack('photoswipe')

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/vendor/sticky-icon/stickyicon.css">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/css/demo7.min.css">
</head>

<body class="home">

    <div class="page-wrapper">
        <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>
        <x-frontend.layouts.partials.header>
            
        </x-frontend.layouts.partials.header>
        <!-- End of Header -->
       
        <!-- start of Main -->
        {{ $slot }}
        <!-- End of Main -->

        <x-frontend.layouts.partials.footer>
            
        </x-frontend.layouts.partials.footer>
        
        <!-- End of Footer -->
    </div>
    <x-frontend.layouts.partials.otherContent>
            
    </x-frontend.layouts.partials.otherContent>
        
	<!-- Plugins JS File -->
    <script src="{{ asset('frontend') }}/vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset('frontend') }}/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="{{ asset('frontend') }}/vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
    <script src="{{ asset('frontend') }}/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <script src="{{ asset('frontend') }}/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="{{ asset('frontend') }}/vendor/jquery.plugin/jquery.plugin.min.js"></script>
    <script src="{{ asset('frontend') }}/vendor/jquery.countdown/jquery.countdown.min.js"></script>
        @stack('photoswipe')

    <!-- Main JS File -->
    <script src="{{ asset('frontend') }}/js/main.min.js"></script>
</body>

</html>