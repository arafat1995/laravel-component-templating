<x-admin.layouts.master>
	<x-slot:title>
		Products Create
	</x-slot:title>

	<x-slot:pageTitle>
	        Products create Page
	</x-slot:pageTitle>

	<div class="row">
        <div class="col-lg-7">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create Product</h1>
                </div>
                <form action="{{ route('product') }}" method="post" class="user">
                	@csrf
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" id="exampleFirstName"
                                placeholder="Category">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-user" id="exampleLastName"
                                placeholder="Sub Category">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="exampleInputEmail"
                            placeholder="Title">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user"
                                id="exampleInputPassword" placeholder="Brand">
                        </div>
                        <div class="col-sm-6">
                            <input type="password" class="form-control form-control-user"
                                id="exampleRepeatPassword" placeholder="SKU">
                        </div>
                    </div>
                    <div class="form-group">
                        	<label>Details</label>
                            <textarea class="form-control form-control-user" placeholder="Describe yourself here..." rows="4" cols="50">
                            	
                            </textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user"
                                id="exampleInputPassword" placeholder="Price">
                        </div>
                        <div class="col-sm-6">
                            <input type="password" class="form-control form-control-user"
                                id="exampleRepeatPassword" placeholder="Old Price">
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="file" class="form-control" style="line-height: 15px;" id="exampleInputEmail" >
                    </div>
                    <button class="btn btn-primary btn-user btn-block">Store</button>
                  
                </form>
                <hr>
               
            </div>
        </div>
    </div>

</x-admin.layouts.master>
