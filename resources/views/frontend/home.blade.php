<x-frontend.layouts.master>
    <x-slot:title>
        Home Page
    </x-slot:title>

    <main class="main">
        <div class="page-content">
            <x-frontend.layouts.partials.sliders/>

            <x-frontend.layouts.partials.bestSellerProducts/>


            

            <section class="banner-group pt-2 mt-10">
                <div class="row cols-md-2 gutter-sm">
                    <div class="banner banner-fixed overlay-light overlay-zoom appear-animate">
                        <figure>
                            <img src="{{ asset('frontend') }}/images/demos/demo7/banners/1.jpg" width="945" height="390" alt="banner" style="background-color: rgb(37, 38, 39);" />
                        </figure>
                        <div class="banner-content y-50">
                            <div class="appear-animate" data-animation-options="{
                                    'name': 'fadeInUpShorter',
                                    'delay': '.2s'
                                }">
                                <h4 class="banner-subtitle text-uppercase text-primary font-weight-bold">
                                    Equipment</h4>
                                <h3 class="banner-title text-white font-weight-bold mb-3">Highly Recommend
                                    Lifestyle Shoes</h3>
                                <p class="font-weight-semi-bold mb-6">Start at &29.00</p>
                                <a href="demo7-shop.html" class="btn btn-primary btn-rounded">Shop Now<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="banner banner-fixed overlay-dark overlay-zoom appear-animate">
                        <figure>
                            <img src="{{ asset('frontend') }}/images/demos/demo7/banners/2.jpg" width="945" height="390" alt="banner" style="background-color: rgb(236, 237, 239);" />
                        </figure>
                        <div class="banner-content y-50">
                            <div class="appear-animate" data-animation-options="{
                                    'name': 'fadeInUpShorter',
                                    'delay': '.3s'
                                }">
                                <h4 class="banner-subtitle text-uppercase text-primary font-weight-bold">
                                    Bestseller</h4>
                                <h3 class="banner-title font-weight-bold mb-3">Latest and Greatest Collection
                                    2021</h3>
                                <p class="font-weight-semi-bold mb-6 text-body">Start at &29.00</p>
                                <a href="demo7-shop.html" class="btn btn-dark btn-rounded">Shop Now<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <x-frontend.layouts.partials.featuresProducts/>
            <x-frontend.layouts.partials.newArrivialsProduct/>
           

            

            <section class="mt-10 pt-6">
                <div class="container">
                    <h2 class="title title-center">From Our Blog</h2>
                    <div class="owl-carousel owl-theme row cols-xl-4 cols-lg-3 cols-sm-2 cols-1" data-owl-options="{
                            'items': 4,
                            'dots': true,
                            'nav': false,
                            'loop': false,
                            'margin': 20,
                            'autoPlay': true,
                            'responsive': {
                                '0': {
                                    'items': 1
                                },
                                '576': {
                                    'items': 2
                                },
                                '992': {
                                    'items': 3
                                },
                                '1200': {
                                    'items': 4,
                                    'dots': false
                                }
                            }
                        }">
                        <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.2s'
                            }">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/blog/1.jpg" width="370" height="255" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    on <a href="#" class="post-date">September 27, 2020</a>
                                    | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                </div>
                                <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                        Images</a></h4>
                                <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                    quissapienfacilisis quis sapien.</p>
                                <a href="post-single.html" class="btn btn-link btn-underline btn-primary btn-md">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.3s'
                            }">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/blog/2.jpg" width="370" height="255" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    on <a href="#" class="post-date">September 27, 2020</a>
                                    | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                </div>
                                <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                        Images</a></h4>
                                <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                    quissapienfacilisis quis sapien.</p>
                                <a href="post-single.html" class="btn btn-link btn-underline btn-primary btn-md">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.4s'
                            }">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/blog/3.jpg" width="370" height="255" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    on <a href="#" class="post-date">September 27, 2020</a>
                                    | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                </div>
                                <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                        Images</a></h4>
                                <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                    quissapienfacilisis quis sapien.</p>
                                <a href="post-single.html" class="btn btn-link btn-underline btn-primary btn-md">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.5s'
                            }">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="{{ asset('frontend') }}/images/demos/demo7/blog/4.jpg" width="370" height="255" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    on <a href="#" class="post-date">September 27, 2020</a>
                                    | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                </div>
                                <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                        Images</a></h4>
                                <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                    quissapienfacilisis quis sapien.</p>
                                <a href="post-single.html" class="btn btn-link btn-underline btn-primary btn-md">Read
                                    More<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="instagram-section mt-10 pt-8 pb-8 mb-10 appear-animate" data-animation-options="{
                    'delay': '.2s'
                }">
                <div class="container">
                    <h2 class="title title-center">Instagram</h2>
                    <div class="owl-carousel owl-theme row cols-xl-6 cols-lg-5 cols-md-4 cols-sm-3 cols-2 gutter-no" data-owl-options="{
                            'items': 6,
                            'nav': false,
                            'dots': false,
                            'autoplay': true,
                            'autoplayTimeout': 5000,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '576': {
                                    'items': 3
                                },
                                '768': {
                                    'items': 4
                                },
                                '992': {
                                    'items': 5
                                },
                                '1200': {
                                    'items': 6
                                }
                            }
                        }">
                        <figure class="instagram">
                            <a href="#">
                                <img src="{{ asset('frontend') }}/images/demos/demo7/instagram/1.jpg" alt="Instagram" width="257" height="257" />
                            </a>
                        </figure>
                        <figure class="instagram">
                            <a href="#">
                                <img src="{{ asset('frontend') }}/images/demos/demo7/instagram/2.jpg" alt="Instagram" width="257" height="257" />
                            </a>
                        </figure>
                        <figure class="instagram">
                            <a href="#">
                                <img src="{{ asset('frontend') }}/images/demos/demo7/instagram/3.jpg" alt="Instagram" width="257" height="257" />
                            </a>
                        </figure>
                        <figure class="instagram">
                            <a href="#">
                                <img src="{{ asset('frontend') }}/images/demos/demo7/instagram/4.jpg" alt="Instagram" width="257" height="257" />
                            </a>
                        </figure>
                        <figure class="instagram">
                            <a href="#">
                                <img src="{{ asset('frontend') }}/images/demos/demo7/instagram/5.jpg" alt="Instagram" width="257" height="257" />
                            </a>
                        </figure>
                        <figure class="instagram">
                            <a href="#">
                                <img src="{{ asset('frontend') }}/images/demos/demo7/instagram/6.jpg" alt="Instagram" width="257" height="257" />
                            </a>
                        </figure>
                    </div>
                </div>
            </section>
        </div>

    </main>
</x-frontend.layouts.master>