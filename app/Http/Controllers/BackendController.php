<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BackendController extends Controller
{
    public function index(){
        return view('backend.dashbord');
    }

    public function login(){
        return view('backend.login');
    }

    public function registration(){
        return view('backend.registration');
    }
}
    
