<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.home');
    }

    public function category()
    {
        return view('frontend.categoryProduct');
    }
    public function porductDetails()
    {
        return view('frontend.productDetails');
    }

    public function cart()
    {
        return view('frontend.cart');
    }

    public function checkout()
    {
        return view('frontend.checkout');
    }

    public function order()
    {
        return view('frontend.order');
    }
}
